#! /usr/bin/env python
# -*- coding: utf-8 -*- 

#	tumblr-downloader.py 	: 		A Python writen script for downloading  
#	Author					:		Philippe 'iamphinson' NDIAYE
#	Email					:		phndiaye@gmail.com

import urllib
import urllib2
import sys
import os
import re
from math import fabs
 
pageStart = 1 		# Page de début
pageEnd = 999		# Page de fin
pageNum = 1 		# Page initiale 
failed_counter = 0	# Nombre de pages sans images 
last_page = '' 		
ver = '1.0'			# Version du script 
 
# Recupère le lien vers le blog (et actualise la page courante)
def get_link(target):
        global pageNum, failed_counter, last_page
 		
        html_url = 'http://' + target + '.tumblr.com/page/' + str(pageNum)		# lien vers la page 
        print('[+] page\t: ' + html_url)
        html_page = get_file(html_url) 		# récupère le contenu de la page 
       
        num_images = get_images(target, html_page) 	# nombres d'images sur la page 
       
        if num_images == 0:
                failed_counter += 1
        if fabs(len(html_page) - len(last_page)) <= 13:
                failed_counter += 1
        if failed_counter == 3:
        		# au bout de 3 pages sans images, le script s'arrête 
        		# un fichier html du site est créé 
                create_htmlFile(target)
                print('\t ' + str(failed_counter) + ' pages sans images')
                raise SystemExit

        last_page = html_page 		# on set la dernière page à celle qui vient d'être traitée   
        pageNum +=1
       
# Retourne le nombre d'images 
def get_images(target, html_page):
        global failed_counter
        # recupères toutes les images du site et set le compteur à 0
        mObj = re.findall(r'<img([^>]*)src=\"([^\"]*)\"([^>]*)>', html_page)   
        count = 0
        # s'il y a des images présentes 
        if mObj:
            for i in mObj:
            	# Pour chaque image
            	# on vérifie qu'il ne s'agit pas de l'avatar, sinon on l'enregistre
            	# puis on incrémente le nombre d'images trouvées
                img = i[1]                    
                if not re.match(r'.*avatar_[a-fA-F0-9]{12}_.*', img):
                        save_file(target, img)
                        count += 1
        else:
            failed_counter += 1

        return count # retourne le nombre d'images trouvées sur le tumblr

# Enregistre l'image            
def save_file(target, url):
	filename = os.path.basename(url)
	if not os.path.isfile(target + '/' + filename):
		print('[+] image\t: ' + url)
		urllib.urlretrieve(url, target + '/' + filename)

# Retourne le contenu du blog 
def get_file(url):
        res = urllib2.urlopen(url)
        html = res.read()
        return html
 
# Affiche l'utilisation du script
def usage():
	print('\tusage : python ' + os.path.basename(sys.argv[0]) + ' [tumblr name] [start page] [end page]\n')
	print('\tex    : python ' + os.path.basename(sys.argv[0]) + ' example1.tumblr.com 1 50')
	       
	raise SystemExit

# Crée un fichier html pour le rendu du blog 
def create_htmlFile(target):
        dirs = os.listdir(target)
       
        fObj = open(target + '.html', "w")
       
        for img in dirs:
                fObj.write('<img src=' + target + '/' + img + ' /><br /><br />')
        fObj.close()
        print('[+] result\t: ' + target + '.html')
               
def main():
	print('\nDownload images from tumblr - ver ' + ver + '\n')       
   
	if(len(sys.argv)==2):
		target = sys.argv[1].lower()
	elif(len(sys.argv)==3):
		target = sys.argv[1]
		pageStart = int(sys.argv[2])
	elif(len(sys.argv)==4):
		target = sys.argv[1]
		pageStart = int(sys.argv[2])
		pageEnd = int(sys.argv[3])
	else:
		usage()
   
	pageNum = pageStart
	target = re.sub(r'\.tumblr\.com','', target)
	target = re.sub(r'http://','', target)
	target = re.sub(r'/','', target)
   
	html_url = 'http://' + target + '.tumblr.com'
	print('[+] site\t: ' + html_url)
	try:
		for page in range(pageStart, pageEnd+1):
			if not os.path.exists(target):
				os.makedirs(target)
			get_link(target)
	except:
		print('\n')

if __name__ == '__main__':
    main()

			